<?php
	include_once(MODELPATH.'/M_FoieGras.php');
	include_once(MODELPATH.'/M_FoieGrasDAO.php');
	include_once(MODELPATH.'/M_Client.php');
	include_once(MODELPATH.'/M_ClientDAO.php');
	include_once(MODELPATH.'/M_VilleDAO.php');

	$foieGrasDAO= FoieGrasDAO::getDAO();
	$clientDAO= ClientDAO::getDAO();
	$villeDAO= VilleDAO::getDAO();

	$json=array('error' => true);
	
	if(isset($_GET['nomCli'])){
		if(isset($_GET['cmpAdresse']) && $_GET['cmpAdresse']  != null)
			$adresse = $_GET['adresseCli'] + ' ' + $_GET['cmpAdresse'];
		else
			$adresse = $_GET['adresseCli'];
		
		$villeId = $villeDAO->villeCPMatch($_GET['villeCli'], $_GET['codePCli']);
		
		$existCli = $clientDAO->existCli($_GET['nomCli'], $_GET['prenomCli']);	
		$client= new Client($_GET['nomCli'], $_GET['prenomCli'], $_GET['mailCli'], $adresse, $villeId, $_GET['telCli']);
		
		if(!$existCli){
			$json['idCli'] = $clientDAO->insertClient($client);
			$json['existCli'] = $existCli;
		}else
			$json['idCli'] = $clientDAO->updateClient($client);	
		
		$_SESSION['client'] = $json['idCli']; 
		
		$json['message']= 'ok';
	}
	echo json_encode($json);
?>