<?php
include_once(MODELPATH.'/M_FoieGras.php');
include_once(MODELPATH.'/M_FoieGrasDAO.php');
include_once(MODELPATH.'/M_PanierDAO.php');

$foieGrasDAO = FoieGrasDAO::getDAO();
$panierDAO = PanierDAO::getDAO();

$data['title'] = "Accueil";
$data['style'] = "accueil.css";

//tout les foies gras 
$data['foiesGras'] = $foieGrasDAO->getFoiesGras();


require_once(HEADER);	
require_once(VIEWPATH."v_accueil.php");
require_once(FOOTER);
?>