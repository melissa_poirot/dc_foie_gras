<?php
include_once(MODELPATH.'/M_FoieGras.php');
include_once(MODELPATH.'/M_FoieGrasDAO.php');
include_once(MODELPATH.'/M_PanierDAO.php');
include_once(MODELPATH.'/M_Commande.php');
include_once(MODELPATH.'/M_CommandeDAO.php');
include_once(MODELPATH.'/M_QuantiteCommande.php');
include_once(MODELPATH.'/M_QuantiteCommandeDAO.php');

//pour résoudre la désactivation de utf8 après validation du formulaire
header('Content-Type: text/html; charset=utf8'); 
 
$foieGrasDAO= FoieGrasDAO::getDAO();
$panierDAO= PanierDAO::getDAO();

$data['style'] = "commande.css";
$data['title'] = "Commande";

$data['foieGrasPanier']= $foieGrasDAO->getAllFoiesGrasById(array_keys($_SESSION['panier']));
$data['debut_commande']='oui';


if($_POST!=null){
	var_dump($_POST);
	$data['debut_commande']='non';
}

require_once(HEADER);	
require_once(VIEWPATH."v_commande.php");
require_once(FOOTER);
?>