<?php
	include_once(MODELPATH.'/M_FoieGras.php');
	include_once(MODELPATH.'/M_FoieGrasDAO.php');
	include_once(MODELPATH.'/M_PanierDAO.php');

	$foieGrasDAO= FoieGrasDAO::getDAO();
	$panierDAO= PanierDAO::getDAO();

	$json=array('error' => true);
	
	if(isset($_GET['id']) && isset($_GET['quantite'])){
		if($foieGrasDAO->existIdFoieGras($_GET['id'])){
			//$panierDAO->addFoieGrasPanier($_GET['id'], $_GET['quantite']);
			if($panierDAO->addFoieGrasPanier($_GET['id'], $_GET['quantite'])){
				$json['message']= 'Le foie gras a été ajouté à votre panier';
				$json['totalPanier']= $panierDAO->getTotal($foieGrasDAO->getAllFoiesGrasById(array_keys($_SESSION['panier'])));
				$json['totalArticle']= $panierDAO->getNbFoieGrasPanier();
				$json['error']=false;
			}else{
				$json['error']=true;
				$json['message']="Désolé, vous ne pouvez plus ajouter ce foie gras à votre panier.";
			}
		}else
			$json['message']= "Le foie gras que vous essayer d'ajouter à votre panier n'existe pas...";
	}
	echo json_encode($json);
?>