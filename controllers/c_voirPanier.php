<?php
include_once(MODELPATH.'/M_FoieGras.php');
include_once(MODELPATH.'/M_FoieGrasDAO.php');
include_once(MODELPATH.'/M_PanierDAO.php');

$foieGrasDAO= FoieGrasDAO::getDAO();
$panierDAO= PanierDAO::getDAO();

$data['style'] = "voirPanier.css";
$data['title'] = "Panier";

if($_SESSION['panier']!=null)
	$data['foieGrasPanier']= $foieGrasDAO->getAllFoiesGrasById(array_keys($_SESSION['panier']));

require_once(HEADER);	
require_once(VIEWPATH."v_voirPanier.php");
require_once(FOOTER);
?>