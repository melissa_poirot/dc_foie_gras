<?php
	include_once(MODELPATH.'/M_FoieGras.php');
	include_once(MODELPATH.'/M_FoieGrasDAO.php');
	include_once(MODELPATH.'/M_Commande.php');
	include_once(MODELPATH.'/M_CommandeDAO.php');

	$foieGrasDAO= FoieGrasDAO::getDAO();
	$commandeDAO= CommandeDAO::getDAO();

	$json=array('error' => true);
	
	if(isset($_GET['etape'])){
		if($_GET['etape']=='validation')
			$json['newEtape']= 'coordonnées';
		
		$json['message']= 'ok';
	}
	echo json_encode($json);
?>