<?php
	include_once(MODELPATH.'/M_FoieGras.php');
	include_once(MODELPATH.'/M_FoieGrasDAO.php');
	include_once(MODELPATH.'/M_PanierDAO.php');

	$foieGrasDAO= FoieGrasDAO::getDAO();
	$panierDAO= PanierDAO::getDAO();

	$json=array('error' => true);
	
	if(isset($_GET['id'])){
		if($foieGrasDAO->existIdFoieGras($_GET['id'])){
			$panierDAO->majPanier($_GET['id'], $_GET['quantite']);
			$json['message']= 'Le panier à été mis à jour';
			$json['totalPanier']= $panierDAO->getTotal($foieGrasDAO->getAllFoiesGrasById(array_keys($_SESSION['panier'])));
			$json['totalFg']= $_GET['quantite']*$_GET['prix'];
			$json['totalArticle']= $panierDAO->getNbFoieGrasPanier();
			$json['error']=false;
		}else
			$json['message']= "Echec de la mise à jour du panier...";
	}
	echo json_encode($json);
?>