<?php
	include_once(MODELPATH.'/M_Ville.php');
	include_once(MODELPATH.'/M_VilleDAO.php');

	$ville= villeDAO::getDAO();

	// = valeur du champ ville
	if(isset($_GET['ville_firstChar'])){
		$char = $_GET['ville_firstChar'];
		$arrayVille= $ville->getVilleChar($char);
		$json['ville']= $arrayVille;
	}else if(isset($_GET['ville'])){
		$json['cp']=$ville->getCPVille($_GET['ville']);
	}else if(isset($_GET['villeTest'])){
		$json['existVille']=$ville->existVille($_GET['villeTest']);
		$villeAutorisees=$ville->getVillesAutorisees();
		$ok=false;
		foreach($villeAutorisees as $v){
			if($_GET['villeTest']==$v){
				$ok=true;
			}
		}
		if(!($ok))
			$json['fraisPort']=15;
		else
			$json['fraisPort']=0;
		
		//si le code postal est défini
		if($_GET['cp'])
			$json['villeMatchCP']= $ville->villeCPMatch($_GET['villeTest'], $_GET['cp']);

	}else if(isset($_GET['cpTest'])){
		$existcp=$ville->existCP($_GET['cpTest']);
		if($existcp){
			$json['villeMatchCP']= $ville->villeCPMatch($_GET['villecp'], $_GET['cpTest']);
		}else{
			$json['villeMatchCP']=false;
		}
			
	}
	echo json_encode($json);
?>