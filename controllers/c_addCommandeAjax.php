<?php
	include_once(MODELPATH.'/M_FoieGras.php');
	include_once(MODELPATH.'/M_FoieGrasDAO.php');
	include_once(MODELPATH.'/M_Commande.php');
	include_once(MODELPATH.'/M_CommandeDAO.php');
	include_once(MODELPATH.'/M_QuantiteCommande.php');
	include_once(MODELPATH.'/M_QuantiteCommandeDAO.php');

	$foieGrasDAO= FoieGrasDAO::getDAO();
	$commandeDAO= CommandeDAO::getDAO();
	$quantiteCommandeDAO= QuantiteCommandeDAO::getDAO();

	$json=array('error' => true);
	
	if(isset($_GET['total'])){
		
		$commande = new Commande();
		$commande->setDateCommande(date('Y-m-d H:i:s'));
		$commande->setClient($_SESSION['client']);
		$commande->setMontant($_GET['total']);
		$commande->setPaye(0);
		
		$_SESSION['panier']['idCommande']= $commandeDAO->insertCommande($commande);
		
		$json['message']= 'ok';
	}
	echo json_encode($json);
?>