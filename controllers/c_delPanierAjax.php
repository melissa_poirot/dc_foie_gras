<?php
	include_once(MODELPATH.'/M_FoieGras.php');
	include_once(MODELPATH.'/M_FoieGrasDAO.php');
	include_once(MODELPATH.'/M_PanierDAO.php');

	$foieGrasDAO= FoieGrasDAO::getDAO();
	$panierDAO= PanierDAO::getDAO();

	$json=array('error' => true);
	
	if(isset($_GET['id'])){
		if($foieGrasDAO->existIdFoieGras($_GET['id'])){
			$panierDAO->deleteFoieGrasPanier($_GET['id']);
			$json['message']= 'Le foie gras a été supprimé de votre panier';
			if($_SESSION['panier']!=null){
				$json['emptyPanier']=0;
				$json['totalPanier']= $panierDAO->getTotal($foieGrasDAO->getAllFoiesGrasById(array_keys($_SESSION['panier'])));
				$json['totalArticle']= $panierDAO->getNbFoieGrasPanier();
			}else
				$json['emptyPanier']=1;
			$json['error']=false;
		}else
			$json['message']= "Le foie gras que vous essayer de supprimer n'existe pas...";
	}
	echo json_encode($json);
?>