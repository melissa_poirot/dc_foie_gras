<?php
class ClientDAO {
		
	private static $instance = null;
	private $bdd;
	
	// singleton
	private function __construct(){ 
		global $bdd;
		$this->bdd = $bdd; 
	}
	
	/**********************************************
	getDAO() -> singleton
	insertClient($nom, $prenom, $mail, $tel, $adresse) -> boolean
	setAbo($bool) -> boolean
	***********************************************/

	//retourne l'instance du singleton
	public static function getDAO(){ 
		if(is_null(self::$instance)){
			self::$instance = new ClientDAO();
		}
		return self::$instance;
	}
	
	//insert
	public function insertClient($client){
		try {
			$req=$this->bdd->prepare('INSERT INTO client (cli_nom, cli_prenom, cli_mail, cli_telephone, cli_adresse, cli_ville) VALUES (:nom, :prenom, :mail, :tel, :adresse, :ville)');
			
			$req=$req->execute(array(
				'nom' => $client->getNom(), 
				'prenom' => $client->getPrenom(),
				'mail' => $client->getMail(),
				'tel' => $client->getTel(),
				'adresse' => $client->getAdresse(),
				'ville' => $client->getVille()
			));	
			
			
			$id = $this->bdd->lastInsertId();
			return $id;	
				

			
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}
	}

	//update 
	public function updateClient($client){
		try {
			$req=$this->bdd->prepare('UPDATE client set cli_mail=:mail, cli_telephone=:tel, cli_adresse=:adresse, cli_ville=:ville WHERE cli_nom= :nom and cli_prenom = :prenom');
			
			$req=$req->execute(array(
				'nom' => $client->getNom(), 
				'prenom' => $client->getPrenom(),
				'mail' => $client->getMail(),
				'tel' => $client->getTel(),
				'adresse' => $client->getAdresse(),
				'ville' => $client->getVille()
			));	
			
			return $this->getIdCli($client->getNom(), $client->getPrenom());
									
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}
	}
	
	public function existCli($nom, $prenom){
		try {
			$req=$this->bdd->query('SELECT cli_id FROM client where cli_prenom="'. $prenom .'" and cli_nom="'. $nom .'"');
						
			$id=$req->fetch(PDO::FETCH_ASSOC);
			
			if(count($req)!=0 && $id['cli_id']!=null)
				return true;
			else
				return false;	
									
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}
	}

	public function getIdCli($nom, $prenom){
		try {
			$req=$this->bdd->query('SELECT cli_id FROM client where cli_prenom="'. $prenom .'" and cli_nom="'. $nom .'"');
			
			$id=$req->fetch(PDO::FETCH_ASSOC);
			
			if(count($req)!=0 && $id['cli_id']!=null)
				return $id['cli_id'];
			else
				return false;
				
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}
	}
	
	
	//set abonnement du client 
	public function setAbo($bool){
		try {
			$req=$this->bdd->prepare('INSERT INTO client (cli_abonne) VALUES (:abo)');
			$req=$req->execute(array('abo' => $bool));	
			return true;
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}
	}
}

?>