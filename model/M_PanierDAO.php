<?php
class PanierDAO {
		
	private static $instance = null;
	private $bdd;
	
	// singleton
	private function __construct(){ 
		global $bdd;
		$this->bdd = $bdd; 
	}
	
	/**********************************************
	getDAO() -> singleton
	addPanier($id, $quantite)
	***********************************************/

	//retourne l'instance du singleton
	public static function getDAO(){ 
		if(is_null(self::$instance)){
			self::$instance = new PanierDAO();
		}
		return self::$instance;
	}
	
	public function addFoieGrasPanier($id, $quantite){
		if(!isset($_SESSION['panier'][$id])){
			$_SESSION['panier'][$id]=$quantite;
			return true;	
		}else if(($quantite + $_SESSION['panier'][$id])<=5){
				$_SESSION['panier'][$id]+=$quantite;
			return true;
		}else 
			return false;
	}
	
	public function changeQuantite($id, $plusOuMoins){
		if($plusOuMoins=='plus')
			$_SESSION['panier'][$id]++;
		else 
			$_SESSION['panier'][$id]--;
	}

	public function deleteFoieGrasPanier($id){
		unset($_SESSION['panier'][$id]);
	}
	
	//prix total du panier des foies gras du panier (foiegrasDAO->getAllFoiesGrasById($array_id))
	public function getTotal($foiesGras){
		$total=0;

		foreach($foiesGras as $fg){
			$total+= ($fg->getPrixKilo()/2)* $_SESSION['panier'][$fg->getId()];
		}
			
		return $total;
	}
			
	public function getNbFoieGrasPanier(){
		return array_sum($_SESSION['panier']);
	}
	
	public function majPanier($idFg, $qt){
		$_SESSION['panier'][$idFg]=$qt;
	}
	
	
	
	
	
	
}

?>