<?php
class CommandeDAO {
		
	private static $instance = null;
	private $bdd;
	
	// singleton
	private function __construct(){ 
		global $bdd;
		$this->bdd = $bdd; 
	}
	
	/**********************************************
	getDAO() -> singleton

	***********************************************/

	//retourne l'instance du singleton
	public static function getDAO(){ 
		if(is_null(self::$instance)){
			self::$instance = new CommandeDAO();
		}
		return self::$instance;
	}
	
	
	//insert
	public function insertCommande($commande){
		try {
			$req=$this->bdd->prepare('INSERT INTO commande (cmd_date, cmd_client, cmd_montant, cmd_validee) VALUES (:date, :client, :montant, :validee)');
			
			$req=$req->execute(array(
				'date' => $commande->getDateCommande(), 
				'client' => $commande->getClient(),
				'montant' => $commande->getMontant(),
				'validee' => $commande->getPaye()
			));	
			
			$id = $this->bdd->lastInsertId();
			return $id;	
				
		
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}
	}
	
	
	//transforme un resultatsql en objet FoieGras
	private function sqlToCommande($requete){
		
		//je creer un nouvel objet FoieGras vide
		$c= new Commande();

		$c->setDateCommande($requete['cmd_date']);
		$c->setId($requete['cmd_id']);
		$c->setClient($requete['cmd_client']);
		$c->setMontant($requete['cmd_montant']);
		$c->setModePaiement($requete['cmd_modePaiement']);
		$c->setPaye($requete['cmd_validee']);
		
		return $c;
	}
	
	
	
	
			
}

?>