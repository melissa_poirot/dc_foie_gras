<?php
class VilleDAO {
		
	private static $instance = null;
	private $bdd;
	
	// singleton
	private function __construct(){ 
		global $bdd;
		$this->bdd = $bdd; 
	}
	
	/**********************************************
	getDAO() -> singleton

	***********************************************/

	//retourne l'instance du singleton
	public static function getDAO(){ 
		if(is_null(self::$instance)){
			self::$instance = new VilleDAO();
		}
		return self::$instance;
	}
	

	public function getVilleChar($firstChar){
		try{
		$req=$this->bdd->query('SELECT ville_nom_reel FROM ville where ville_nom_reel like "'.$firstChar.'%"');

		$lesVilles= array();
		
		if(count($req)!=0){
			while($v=$req->fetch(PDO::FETCH_ASSOC)){ 
				$lesVilles[] = $v['ville_nom_reel'];
			}
		}
		
		return $lesVilles;
		
								
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}	
	}
	
	public function existVille($nomVille){
		try{
		$req=$this->bdd->query('SELECT ville_id FROM ville where ville_nom_reel="'.$nomVille.'"
								or ville_nom_simple ="'.$nomVille.'"');

		if($req->fetch()!=null)
			return true;
		else
			return false;		
								
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}	
	}
	
	
	public function existCP($cp){
		try{
		$req=$this->bdd->query('SELECT ville_id FROM ville where ville_code_postal="'.$cp.'"');
		
		if($req->fetch()!=null)
			return true;
		else
			return false;			
								
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}	
	}

	//renvoi id si trouve sinon renvoi false
	public function villeCPMatch($villeNom, $cp){
		try{
			$req=$this->bdd->query('SELECT ville_id FROM ville where ville_code_postal="'.$cp.'" 
									and (ville_nom_simple="'.$villeNom.'" or ville_nom_reel="'.$villeNom.'")');
			
			$id=$req->fetch(PDO::FETCH_ASSOC);	
			
			if(count($req)!=0 && $id!=null){	
				return $id['ville_id'];
			}else
				return false;		
								
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}	
	}
	
	
	//retourne le code postal correspondant a $ville
	public function getCPVille($v){
		try{
		$req=$this->bdd->query('SELECT ville_code_postal FROM ville where ville_nom_simple="'.$v.'" or ville_nom_reel="'.$v.'"');
		
		if(count($req)!=0)
			$cp=$req->fetch(PDO::FETCH_ASSOC);
		
		return $cp['ville_code_postal'];
		
								
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}	
	}

	//retourne un tableau de ville ou le frais de port est offert
	public function getVillesAutorisees(){
		try{
		$req=$this->bdd->query('SELECT ville_nom_reel, ville_nom_simple FROM ville where ville_autorisation=1');
		$lesVilles= array();
		
		if(count($req)!=0)
		while($v=$req->fetch(PDO::FETCH_ASSOC)){
			$lesVilles[]=$v['ville_nom_reel'];
			$lesVilles[]=$v['ville_nom_simple'];
		}
		
		return $lesVilles;
		
								
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}	
	}	
	
	
	//transforme un resultatsql en objet Ville
	private function sqlToVille($requete){
	
		$v= new Ville();

		$v->setId($requete['ville_id']);
		$v->setNomSimple($requete['ville_nom_simple']);
		$v->setNomReel($requete['ville_nom_reel']);
		$v->setCodePostal($requete['ville_code_postal']);

		return $v;
	}
		
}

?>