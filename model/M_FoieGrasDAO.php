<?php
class FoieGrasDAO {
		
	private static $instance = null;
	private $bdd;
	
	// singleton
	private function __construct(){ 
		global $bdd;
		$this->bdd = $bdd; 
	}
	
	/**********************************************
	getDAO() -> singleton
	getFoiesGras() -> [] FoieGras
	getFoieGrasByNom($nom) -> FoieGras 
	***********************************************/

	//retourne l'instance du singleton
	public static function getDAO(){ 
		if(is_null(self::$instance)){
			self::$instance = new FoieGrasDAO();
		}
		return self::$instance;
	}
	
	//retourne tout les foies gras
	public function getFoiesGras(){
		try {
			$req=$this->bdd->query('SELECT * FROM foiegras');
			$lesFoiesGras=array();

			if(count($req)!=0){
				while($fg=$req->fetch()){
					//liste dobjet FoieGras
					$lesFoiesGras[] = $this->sqlToFoieGras($fg);
				}
			}

			return $lesFoiesGras;
									
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}
	}

	//retourne un foie gras en fonction de son nom
	public function getFoieGrasBySaveur($s){
		try{
			$req=$this->bdd->query('SELECT * FROM foiegras where fg_saveur="'.$s.'"');

			if(count($req)!=0)
				$leFoieGras= $this->sqlToFoieGras($req->fetch());
			
			return $leFoieGras;
									
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}
	}

	//retourne un foie gras en fonction de son id
	public function getFoieGrasById($id){
		try{
			$req=$this->bdd->query('SELECT * FROM foiegras where fg_id='.$id);

			if(count($req)!=0)
				$leFoieGras= $this->sqlToFoieGras($req->fetch());
			
			return $leFoieGras;
									
		}catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}
	}
	
	//retourne des foie gras en fonction des id
	public function getAllFoiesGrasById($array_id){
		try{
			$req=$this->bdd->query('SELECT * FROM foiegras where fg_id in('.implode($array_id, ', ').')');
			$lesFoiesGras=array();
			
			if(count($req)!=0){
				while($fg=$req->fetch()){
					//liste dobjet FoieGras
					$lesFoiesGras[] = $this->sqlToFoieGras($fg);
				}									
			}
			return $lesFoiesGras;
		}
		catch(PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}
	}
	
	
	public function existIdFoieGras($id){
		try {
			$req=$this->bdd->prepare('SELECT fg_id FROM foiegras WHERE fg_id =:id');
			
			$req=$req->execute(array('id' => $id));	
			
			if($req!=null)
				return true;
			else
				return false;
		}catch(PDOException $e) {
			die("PDO Error existIdFoieGras:".$e->getMessage());
		}
	}
		
	//transforme un resultatsql en objet FoieGras
	private function sqlToFoieGras($requete){
		
		//je creer un nouvel objet FoieGras vide
		$fg= new FoieGras();

		$fg->setId($requete['fg_id']);
		$fg->setLibelle($requete['fg_libelle']);
		$fg->setPreparation($requete['fg_preparation']);
		$fg->setPrixKilo($requete['fg_prixKilo']);
		$fg->setSaveur($requete['fg_saveur']);
		$fg->setGarantieIGPSO($requete['fg_garantieIGPSO']);
		$fg->setStock($requete['fg_quantiteStock']);
		

		return $fg;
	}
	
		
	public function remove_accents($str, $charset='utf-8')
	{
		$str = htmlentities($str, ENT_NOQUOTES, $charset);
		
		$str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
		$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
		$str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
		
		return $str;
	}
			
}

?>