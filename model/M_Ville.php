<?php

class Ville{
	
	private $id;
	private $nomSimple;
	private $nomReel;
	private $codePostal;

	
	//GETTERS
	
	public function getId(){return $this->id;}
	public function getNomSimple(){return $this->nomSimple;}
	public function getNomReel(){return $this->nomReel;}
	public function getCodepostal(){return $this->codePostal;}


	//SETTERS
	public function setId($id){$this->id=$id;}
	public function setNomSimple($ns){$this->nomSimple=$ns;}
	public function setNomReel($nr){$this->nomReel=$nr;}
	public function setCodePostal($cp){$this->codePostal=$cp;}
	
}
?>