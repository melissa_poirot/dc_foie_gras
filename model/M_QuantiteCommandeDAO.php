<?php
class QuantiteCommandeDAO{
		
	private static $instance = null;
	private $bdd;
	
	// singleton
	private function __construct(){ 
		global $bdd;
		$this->bdd = $bdd; 
	}
	
	/**********************************************
	getDAO() -> singleton

	***********************************************/

	//retourne l'instance du singleton
	public static function getDAO(){ 
		if(is_null(self::$instance)){
			self::$instance = new QuantiteCommandeDAO();
		}
		return self::$instance;
	}
	
	
	
	//transforme un resultatsql en objet FoieGras
	private function sqlToQuantiteCommande($requete){
		
		//je creer un nouvel objet FoieGras vide
		$c= new QuantiteCommande();

		$c->setCommande($requete['qc_commande']);
		$c->setFoieGras($requete['qc_foieGras']);
		$c->setQuantite($requete['qc_quantite']);
		
		return $c;
	}
	
			
}

?>