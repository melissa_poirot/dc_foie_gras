<?php

class FoieGras{
	
	private $id;
	private $libelle;
	private $preparation;
	private $prixKilo;
	private $saveur;
	private $garantieIGSO;
	private $stock;

	
	//GETTERS
	
	public function getId(){return $this->id;}
	public function getLibelle(){return $this->libelle;}
	public function getPreparation(){return $this->preparation;}
	public function getPrixKilo(){return $this->prixKilo;}
	public function getSaveur(){return $this->saveur;}
	public function getGarantieIGPSO(){return $this->garantieIGSO;}
	public function getStock(){return $this->stock;}



	//SETTERS
	public function setId($id){$this->id=$id;}
	public function setLibelle($l){$this->libelle=$l;}
	public function setPreparation($p){$this->preparation=$p;}
	public function setPrixKilo($prix){$this->prixKilo=$prix;}
	public function setSaveur($saveur){$this->saveur=$saveur;}
	public function setGarantieIGPSO($g){$this->garantieIGSO=$g;}
	public function setStock($st){$this->stock=$st;}

}
?>