<?php

class Client{
	
	private $id;
	private $nom;
	private $prenom;
	private $mail;
	private $adresse;
	private $ville;	//ojb ville
	private $tel;
	private $abonne;

	
		
    public function __construct($nom, $prenom, $mail, $adresse, $ville, $tel){
        $this->nom = $nom;
        $this->prenom = $prenom;
		$this->mail = $mail;
        $this->adresse = $adresse;
		$this->ville = $ville;
		$this->tel = $tel;
    }
	
	//GETTERS
	
	public function getId(){return $this->id;}
	public function getNom(){return $this->nom;}
	public function getPrenom(){return $this->prenom;}
	public function getMail(){return $this->mail;}
	public function getAdresse(){return $this->adresse;}
	public function getVille(){return $this->ville;}
	public function getTel(){return $this->tel;}
	public function getAbonne(){return $this->abonne;}


	//SETTERS
	public function setId($id){$this->id=$id;}
	public function setNom($n){$this->nom=$n;}
	public function setPrenom($p){$this->prenom=$p;}
	public function setMail($m){$this->mail=$m;}
	public function setTel($t){$this->tel=$t;}
	public function setAbonne($a){$this->abonne=$a;}
	
}
?>