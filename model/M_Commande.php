<?php

class Commande{
	
	private $id;
	private $dateCommande;
	private $client; //id client
	private $montant;
	private $modePaiement;
	private $paye;	// la commande a été payé ou pas

	
	//GETTERS
	
	public function getId(){return $this->id;}
	public function getDateCommande(){return $this->dateCommande;}
	public function getClient(){return $this->client;}
	public function getMontant(){return $this->montant;}
	public function getModePaiement(){return $this->modePaiement;}
	public function getPaye(){return $this->paye;}


	//SETTERS
	public function setId($id){$this->id=$id;}
	public function setDateCommande($d){$this->dateCommande=$d;}
	public function setClient($c){$this->client=$c;}
	public function setMontant($m){$this->montant=$m;}
	public function setModePaiement($mp){$this->modePaiement=$mp;}
	public function setPaye($p){$this->paye=$p;}


}
?>