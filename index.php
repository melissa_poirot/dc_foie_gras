<?php

/* @author Poirot Melissa
 * Implementation d'un contrôleur frontal.
 *
 * Ce programme évalue la valeur du parameter 'action' de la requête
 * HTTP afin de charger le controleur approprié.
 *
 * On utilise aussi ce point d'entrée pour définir quelques constantes
 * utiles.
 */

if(!(isset($_SESSION))){
	session_start();
	if(!(isset($_SESSION['panier'])))
		$_SESSION['panier']=array();

	if(!(isset($_SESSION['client'])))
		$_SESSION['client']=null;
}
//connexion a la base de donnees
try {
	$bdd = new PDO('mysql:host=localhost;dbname=foiegras;charset=utf8', 'root', 'root');
				$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (\Exception $e) {
	echo "La connexion à la base de données a echouée, merci de bien vouloir configurer sur 'index.php (l.26)'";
}



//TEST

//var_dump($_SESSION['panier']);


// Définition des constantes
define('APPPATH', getcwd() . '/');
define('CONTROLPATH', APPPATH . 'controllers/');
define('DATAPATH', APPPATH . 'data/');
define('DATABASE', DATAPATH . 'foiegras.sql');
define('MODELPATH', APPPATH . 'model/');
define('VIEWPATH', APPPATH . 'views/');
define('STYLEPATH', VIEWPATH . 'styles/');
define('IMGPATH', VIEWPATH . 'images/');
define('TPLTPATH', VIEWPATH . 'template/');
define('HEADER', TPLTPATH . 'header.php');
define('FOOTER', TPLTPATH . 'footer.php');
define('SCRIPT', APPPATH . 'js/');

// Définition des controleurs pour chaque 'action'
$controllers = array(
    'accueil' => 'c_accueil.php',
	'consulterDetails' => 'c_voirFoieGras.php',
	'ajouterPanier' => 'c_addPanier.php',
	'consulterPanier' => 'c_voirPanier.php',
	'quiSuisJe' => 'c_quiSuisJe.php',
	'conseilsPro' => 'c_conseils.php',
	'paiementEtLivraison' => 'c_paiement.php',
	'passerCommande' => 'c_commande.php',
	'addPanierAjax' => 'c_addPanierAjax.php',
	'delPanierAjax' => 'c_delPanierAjax.php',
	'changeQuantiteAjax' => 'c_changeQuantiteAjax.php',
	'changeEtapeAjax' => 'c_changeEtapeAjax.php',
	'autocompletion' => 'c_autocompletion.php',
	'insertCommande' => 'c_addCommandeAjax.php',
	'insertClient'	=>	'c_addClientAjax.php'

);

$default_controller = $controllers['accueil'];

// Détermination du controlleur à charger
$ctrlr = $default_controller;
if (isset($_GET['action'])) {
    if (isset($controllers[$_GET['action']])) {
        $ctrlr = $controllers[$_GET['action']];
    } else {
        die('Vous devez définir le controleur correspondant à action=' . $_GET['action']
                . ' dans le tableau $controllers du fichier index.php');
    }
}

// Chargement du controleur
if (file_exists(CONTROLPATH . $ctrlr)) {
    require_once(CONTROLPATH . $ctrlr);
} else {
    die("Vous avez définit le controleur " . $ctrlr
            . " pour traiter l'action=" . $_GET['action']
            . " cependant ce fichier n'existe pas dans le repertoire " . CONTROLPATH);
}

?>
