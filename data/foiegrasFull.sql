-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 22 Juin 2016 à 16:54
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `foiegras`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `cli_id` int(11) NOT NULL AUTO_INCREMENT,
  `cli_nom` varchar(100) NOT NULL,
  `cli_prenom` varchar(100) NOT NULL,
  `cli_mail` varchar(200) NOT NULL,
  `cli_adresse` varchar(100) NOT NULL,
  `cli_ville` varchar(45) NOT NULL,
  `cli_telephone` int(10) NOT NULL,
  `cli_abonne` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`cli_id`),
  KEY `cli_ville` (`cli_ville`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE IF NOT EXISTS `commande` (
  `cmd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cmd_date` datetime NOT NULL,
  `cmd_client` int(11) NOT NULL COMMENT 'id client ',
  `cmd_montant` float NOT NULL,
  `cmd_modePaiement` set('Paypal','À la livraison') NOT NULL,
  PRIMARY KEY (`cmd_id`),
  KEY `cmd_client` (`cmd_client`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `foiegras`
--

CREATE TABLE IF NOT EXISTS `foiegras` (
  `fg_id` int(11) NOT NULL AUTO_INCREMENT,
  `fg_libelle` varchar(100) NOT NULL,
  `fg_preparation` set('Cru','Mi-cuit','Cuit','Mi-cuit sous vide','Cuit sous vide') NOT NULL,
  `fg_prixKilo` float NOT NULL,
  `fg_saveur` set('Cognac','Armagnac','Ail fumé') NOT NULL,
  `fg_garantieIGPSO` tinyint(1) NOT NULL COMMENT 'le foie gras est il certifié IG sud ouest',
  `fg_quantiteStock` int(11) DEFAULT NULL,
  PRIMARY KEY (`fg_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `foiegras`
--

INSERT INTO `foiegras` (`fg_id`, `fg_libelle`, `fg_preparation`, `fg_prixKilo`, `fg_saveur`, `fg_garantieIGPSO`, `fg_quantiteStock`) VALUES
(1, 'Lobe de foie gras', 'Mi-cuit sous vide', 100, 'Cognac', 1, NULL),
(2, 'Lobe de foie gras', 'Mi-cuit sous vide', 100, 'Armagnac', 1, NULL),
(3, 'Lobe de foie gras', 'Mi-cuit sous vide', 100, 'Ail fumé', 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `quantitecommande`
--

CREATE TABLE IF NOT EXISTS `quantitecommande` (
  `qc_commande` int(11) NOT NULL COMMENT 'id commande',
  `qc_foieGras` int(11) NOT NULL COMMENT 'id foie gras',
  `qc_quantite` int(11) NOT NULL,
  PRIMARY KEY (`qc_commande`,`qc_foieGras`,`qc_quantite`),
  KEY `fk_fg_id` (`qc_foieGras`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_cli_id` FOREIGN KEY (`cmd_client`) REFERENCES `client` (`cli_id`);

--
-- Contraintes pour la table `quantitecommande`
--
ALTER TABLE `quantitecommande`
  ADD CONSTRAINT `fk_cmd_id` FOREIGN KEY (`qc_commande`) REFERENCES `commande` (`cmd_id`),
  ADD CONSTRAINT `fk_fg_id` FOREIGN KEY (`qc_foieGras`) REFERENCES `foiegras` (`fg_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
