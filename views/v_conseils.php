<div class="page" id=<?php echo $data['id'];?> >
    <header class="titrePage">
		<h1>Conseils professionnels</h1>
	</header>
	<section id="conservation">
		<h3>Conservation</h3>
		<p>La technique de cuisson sous vide permet de garder le produit trois semaines au frais sans être ouvert.
			Après ouverture, le foie gras sera a consommé dans les trois jours qui suivent.
		</p>
	</section>
	<section id="Accompagnements">
		<h3>Accompagnements</h3>
		<h4>Confiture d'oignon</h4>
		<hr color=#80000></hr>
		<img class='imgRecette' src="views/images/confiture_oignon.jpg" alt="confiture_oignon" >
		<div class="recette">
			<h5>La recette</h5>
			<ul>
				<li>1kg d'oignon</li>
				<li>500g de sucre</li>
			</ul>
			<ol>
				<li>Épluchez et hachez très finement les oignon</li>
				<li>Ajoutez le sucre et les oignons dans une casserole</li>
				<li>Laissez cuire à feu doux jusqu'à otention d'une confiture</li>
			</ol>
		</div>
		<h4>Chutney de figues</h4>
		<hr color=#80000></hr>
		<img class='imgRecette' src="views/images/chutney_figues.png" alt="chutney_figues" >
		<div class="recette">
			<h5>La recette</h5>
			<ul>
				<li>500g de figues</li>
				<li>2 oignons</li>
				<li>1 cuillère de gingembre frais rapé</li>
				<li>100g de raisins secs</li>
				<li>250ml de vinaigre de xérès</li>
				<li>250g de sucre roux</li>
				<li>250ml d'eau</li>
				<li>1 picée sel</li>
			</ul>
			<ol>
				<li>Lavez les figues et les couper en morceaux</li>
				<li>Épluchez et émincer les oignons</li>
				<li>Ajoutez le vinaigre, l'eau, le sucre, le sel et le gingembre dans une casserole puis</br> &nbsp; &nbsp; &nbsp; faire frémir</li> <!-- &nbsp; = espace-->
				<li>Ajoutez les figues, les raisins et les oignons</li>
				<li>Laissez cuire environ 45 minutes à feu doux</li>
			</ol>	
		</div>
		<h4>Gelée au madère</h4>
		<hr color=#80000></hr>
		<img class='imgRecette' src="views/images/gelee_madere.jpg" alt="gelée_madère" >
		<div class="recette">
			<h5>La recette</h5>
			<ul>
				<li>100ml madère</li>
				<li>150ml d'eau</li>
				<li>3 feuilles de gelatine</li>
				<li>sel</li>
				<li>poivre</li>
			</ul>
			<ol>
				<li>Faites bouillir l'eau et le madère</li>
				<li>Salez et poivrez</li>
				<li>Mettez la gelatine à tremper dans de l'eau foide</li>
				<li>Lorsque la gelatine a ramoli, l'imcorporer au mélange d'eau et de madère hors du</br> &nbsp; &nbsp; &nbsp; feu</li>
				<li>Débarassez au frais puis coupez en petit morceau</li>
			</ol>	
		</div>
		<h4>Les conseils du sommelier</h4>
		<ul>
			<li>Verdesse de chez Thomas Finot</li>
			<li>Gewurstraminer<dt>un vin des côteaux alsacien qui vous offrira un florilège de saveurs de fruit et d'épice</dt></li>
			<li>Pinot noir d'alsace<dt>peu tannique et parfumé de notes fruitées</dt></li>
			<li>Pinot gris d'alsace<dt>vin charpenté, rond et long en bouche dont les arôme se marient parfaitement avec le foie gras</dt></li>
		</ul>
		<p>Et pour les amateurs de vins plus sucrés, vous pouvez optés pour les vins suivant:</p> 
		<ul>
			<li>Sauternes</li>
			<li>Montbazillac</li>
			<li>Jurançon</li>
			<li>Vouvray</li>
		</ul>
		<p>Vous pouvez également osez le champagne qui se marie lui aussi très bien avec le foie gras.</p>
	</section>
	<h3>Idées de dressage</h3>
	
	<span id="sl_i1" class="sl_command sl_i">&nbsp;</span>
	<span id="sl_i2" class="sl_command sl_i">&nbsp;</span>
	<span id="sl_i3" class="sl_command sl_i">&nbsp;</span>
	<span id="sl_i4" class="sl_command sl_i">&nbsp;</span>
	
	
	<section id="slideshow">
	
		<a class="commands prev commands1" href="#sl_i4">&lt;</a>
		<a class="commands next commands1" href="#sl_i2" >&gt;</a>
		<a class="commands prev commands2" href="#sl_i1" >&lt;</a>
		<a class="commands next commands2" href="#sl_i3" >&gt;</a>
		<a class="commands prev commands3" href="#sl_i2" >&lt;</a>
		<a class="commands next commands3" href="#sl_i4" >&gt;</a>
		<a class="commands prev commands4" href="#sl_i3" >&lt;</a>
		<a class="commands next commands4" href="#sl_i1" >&gt;</a>
		
		
		
		<div class="container">
			<div class="c_slider"></div>
			<div class="slider">
				<figure>
					<img src="views/images/chat1.jpg" width="640" height="310" />
				</figure><!--
				--><figure>
					<img src="views/images/chat2.jpg"  width="640" height="310" />
				</figure><!--
				--><figure>
					<img src="views/images/chat3.jpg" width="640" height="310" />
				</figure><!--
				--><figure>
					<img src="views/images/chat4.jpg" width="640" height="310" />
				</figure>
			</div>
		</div>
		
		<span id="timeline"></span>
		
		<ul class="dots_commands"><!--
			--><li><a title="Show slide 1" href="#sl_i1">Slide 1</a></li><!--
			--><li><a title="Show slide 2" href="#sl_i2">Slide 2</a></li><!--
			--><li><a title="Show slide 3" href="#sl_i3">Slide 3</a></li><!--
			--><li><a title="Show slide 4" href="#sl_i4">Slide 4</a></li>
		</ul>
		
	</section>
</div>