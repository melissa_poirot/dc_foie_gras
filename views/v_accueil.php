<script>
	function hover(element){
		var id="imgFoieGras"  + element.id;
		var img=document.getElementById(id);
		
		img.setAttribute('src', 'views/images/top_left_corner_decoration_change.png');
	}
	function unhover(element) {
		var id="imgFoieGras" + element.id;
		var img=document.getElementById(id);
		
		img.setAttribute('src', 'views/images/top_left_corner_decoration.png');
	}
	
	function changePrix(element){
		id="#prix"+element.id.substr(14, 3);
		var quantite=new Number($("#"+element.id).val());
		var prixBalotine=new Number(<?php echo $data['foiesGras'][0]->getPrixKilo()/2; ?>);
		
		var prix= quantite * prixBalotine;
		$(id).text(prix+" €");
	}
	
	function addPanier(element, id){
		var quant=parseInt($("#selectQuantite"+element.id.substr(6, 3)).val());

		$.get("?action=addPanierAjax&id="+id+"&quantite="+quant,{},function(data){
				//alert(data.message);
				if(!(data.error)){
					$("#alertAddPanier").show();
					$("#lienPanier").show();
					$("#panierVide").hide();
					
					$("#totalArticle").empty().append(data.totalArticle + " ballotine(s)");
					$("#totalPanier").empty().append("_" + data.totalPanier + " €");
	
				}else
					$("#alertAddPanierError").show();
				
					
				
		}, 'json');
	}
	
	function hideAlert(id){
		$("#"+id).hide();
	}
	
	function link(location){
		document.location.href="?action="+location;
	}
	
</script>


<div class="page" id=<?php echo $data['title'];?> >
<header class="titrePage"><h1></h1></header>
	<div id='presentation_foie_gras'>
		<h2>Lobes de foie gras frais entiers</h2>
		<div id='caracteristiqueFG'>
			<p>
				<img id="icon_casserole" src="views/images/icon_casserole.png" alt="icon_casserole"> mi-cuits sous vide 
			</p>
			<p>
				<img id="icon_saveur" src="views/images/icon_saveur.png" alt="icon_saveur"> 3 saveurs 
			</p>
			<p>
				<img id="icon_certification" src="views/images/icon_certification.png" alt="icon_certification"> certifié IGP Sud-Ouest
			</p>
		</div>
		<p id='prix_balotine'>
			<?php echo $data['foiesGras'][0]->getPrixKilo()/2; ?>€ la ballotine de 500g
		</p>
	</div>
	<section id="lesSaveurs">
		<h3>Choisissez votre saveur</h3>
		<?php foreach($data['foiesGras'] as $unFoieGras){ ?>
			<img class="top_left_corner_decoration" id="imgFoieGras<?php echo substr($unFoieGras->getSaveur(), 0, 3); ?>" src="views/images/top_left_corner_decoration.png" alt="top_left_corner_decoration">
			<div class="unFoieGras" id="<?php echo substr($unFoieGras->getSaveur(), 0, 3); ?>" onmouseover="hover(this)" onmouseout="unhover(this)" >
				<h4>Saveur <?php echo $unFoieGras->getSaveur(); ?></h4>
				<hr align=center>
				<p>Quantité: <select onChange="changePrix(this)" id="selectQuantite<?php echo substr($unFoieGras->getSaveur(), 0, 3);?>" class="selectQuantite<?php echo substr($unFoieGras->getSaveur(), 0, 3);?>"> 
							<?php 		
								for($nb=1; $nb<6; $nb++)
									echo "<option>".$nb."</option>"; ?>
							</select>
				</p> 
				<p id='prix<?php echo substr($unFoieGras->getSaveur(), 0, 3); ?>'><b>50 €</b></p>
				
				<span class='button' id='button<?php echo substr($unFoieGras->getSaveur(), 0, 3);?>' onClick='addPanier(this, <?php echo $unFoieGras->getId(); ?>)'>
					Ajouter au panier
				</span>
			</div>
		<?php } ?>
	
	</section>
	<div id="alertAddPanier" hidden>
	
		<img class="croix" src="views/images/croix.png" alt="croix" onClick='hideAlert("alertAddPanier")'>
		<p id='alertMessage'>Le foie gras a été ajouté à votre panier.</br></br> Voulez-vous consulter votre panier?</p>	
		
		<span class='button' id='valide' onClick='link("consulterPanier")'>
			Commander
		</span>
		<span class='button' id='annule' onClick='hideAlert("alertAddPanier")'>
			Continuer mes achats
		</span>
	</div>
	
	<div id="alertAddPanierError" hidden>
	
		<img class="croix" src="views/images/croix.png" alt="croix" onClick='hideAlert("alertAddPanierError")'>
		<p id="alertMessageError">Désolé, la commande est limité à 5 balottines par foie gras</p>	
		<span class='button' onClick='hideAlert("alertAddPanierError")'>
			OK
		</span>
	</div>

</div>
