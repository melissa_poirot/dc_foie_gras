<script>
	function selectNav(){
		var liAccueil=document.getElementById('liAccueil');
		var liConseils=document.getElementById('liConseils');
		var liQui=document.getElementById('liQui');
		var liPaiement=document.getElementById('liPaiement');
		if(<?php echo '"'.$_GET['action'].'"';?>=='accueil')
			liAccueil.style.borderTop='6px solid #BA624D';
		else if(<?php echo '"'.$_GET['action'].'"';?>=='conseilsPro')
			liConseils.style.borderTop='6px solid #BA624D';
		else if(<?php echo '"'.$_GET['action'].'"';?>=='quiSuisJe')
			liQui.style.borderTop='6px solid #BA624D';
		else if(<?php echo '"'.$_GET['action'].'"';?>=='paiementEtLivraison')
			liPaiement.style.borderTop='6px solid #BA624D';
	}
	
	function link(location){
		document.location.href="?action="+location;
	}
	
</script>

<?php 
	//total du panier
	if($_SESSION['panier']!=null){
		require_once(MODELPATH.'/M_PanierDAO.php');
		require_once(MODELPATH.'/M_FoieGrasDAO.php');
		require_once(MODELPATH.'/M_FoieGras.php');

		$foieGrasDAO = FoieGrasDAO::getDAO();
		$panierDAO = PanierDAO::getDAO();
		
		$totalPanier= $panierDAO->getTotal($foieGrasDAO->getAllFoiesGrasById(array_keys($_SESSION['panier'])));
		$totalArticle= $panierDAO->getNbFoieGrasPanier();
	}
	
?>
<html>
    <head>
		<title> 
            <?php
            if (isset($data['title'])) {
                echo $data['title'];
            } else {
                echo "Welcome !";
            }
            ?>
        </title>
		<link rel="shortcut icon" href="views/images/logo2.gif" type="image/x-icon"/>
		<link rel="icon" href="views/images/logo2.gif" type="image/x-icon"/>
		<link rel="stylesheet" href="views/styles/header.css" type="text/css" />
		<link rel="stylesheet" href="views/styles/footer.css" type="text/css" />
		<link rel="stylesheet" href="views/styles/body.css" type="text/css" />
		<!-- google font-->
		<link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Bevan' rel='stylesheet' type='text/css'>
	
        <?php 
        if (isset($data['style'])) {
            echo '<link href="views/styles/' . $data['style'] . '" rel="stylesheet" type="text/css" />';
        }
        ?>
        <meta charset="UTF-8" >
        
		<script src="../data/jquery-1.11.2.min.js"></script>
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script type="text/javascript" language="Javascript" src="js/jquery.js"></script>
		
		
    </head>

    <header id="header">
		<img id="titre" src="views/images/banniere_2.jpg" alt="Accueil" >
		
		<div id='div_panier'>
			<div id="div_panier2" >
				<a id='linkPanier' alt="VoirPanier" href="?action=consulterPanier">
					<img id="logoPanier" src="views/images/panier2.png" style="width:60px;height:70px" alt="Panier">
				</a>
				<h2>Votre panier</h2>
				<div id='contenuPanier'>					
					<?php 
						if($_SESSION['panier']!=NULL){ 
							echo '<span id="totalArticle">'. $totalArticle .' ballotine(s)</span><span id="totalPanier">_'. $totalPanier .' €</span>';
							echo '<a id="lienPanier" alt="VoirPanier" href="?action=consulterPanier">Consulter le panier</a>';
						}
						else{
							echo '<span id="totalArticle"></span><span id="totalPanier"></span>';
							echo '<a id="lienPanier" alt="VoirPanier" href="?action=consulterPanier" hidden>Consulter le panier</a>';
							echo '<span id="panierVide">Votre panier est vide<span>';
						}
					?>
				</div>
			</div>
		</div>
		
		<nav id="menu">
			<ul>
				<li id='liPaiement' onClick="link('paiementEtLivraison')">Paiement - livraison</li>
				<li id='liConseils' onClick="link('conseilsPro')">Conseils professionnels</li>
				<li id='liQui' onClick="link('quiSuisJe')">Qui suis-je?</li>
				<li id='liAccueil' onClick="link('accueil')">Accueil</li>
			</ul>
		</nav>
	</header> 
	
<body onload="selectNav()">
