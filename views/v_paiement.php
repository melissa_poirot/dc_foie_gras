<div class="page" id=<?php echo $data['id'];?> >
    <header class="titrePage"><h1>Paiement et livraison</h1></header>
	<section id="paiement">
		<h3>Paiement</h3>
		<p>Par PayPal.</p>
		<p>Par chèque ou liquide à la livraison de votre commande.</p>
	</section>
	<section id="livraison">
		<h3>Livraison</h3>
		<h4>Dans la limite de 10km autour de Grenoble: </h4>
			<p>livraison à domicile effectuée par mes soins sous deux jours après la confirmation de votre commande. Voici la liste des villes et villages concernés: 
			</p>
			<ul>
				<li>Biviers</li>
				<li>Bresson</li>
				<li>Brié-et-Angonnes</li>
				<li>Champagnier</li>
				<li>Claix</li>
				<li>Corenc</li>
				<li>Domène</li>
				<li>Échirolles</li>
				<li>Engins</li>
				<li>Eybens</li>
				<li>Fontaine</li>
				<li>Fontanil-Cornillon</li>
				<li>Gières</li>
				<li>Grenoble</li>
				<li>Herbeys</li>
				<li>Jarrie</li>
				<li>La Tronche</li>
				<li>Le Pont-de-Claix</li>
				<li>Le Sappey-en-Chartreuse</li>
				<li>Meylan</li>
				<li>Montbonnot-Saint-Martin</li>
				<li>Murianette</li>
				<li>Poisat</li>
				<li>Proveysieux</li>
				<li>Quaix-en-Chartreuse</li>				
				<li>Saint-Égrève</li>
				<li>Saint-Martin-d'Hères</li>
				<li>Saint-Martin-d'Uriage</li>
				<li>Saint-Martin-le-Vinoux</li>
				<li>Saint-Nizier-du-Moucherotte</li>
				<li>Sarcenas</li>
				<li>Sassenage</li>
				<li>Sessinet-Pariset</li>
				<li>Seyssins</li>
				<li>Venon</li>	
			</ul>
		<h4>Pour toutes distances suppérieures: </h4>
			<p>colis envoyé par chronopost en sac isoterm accompagné de glace pour le respect de la chaine du froid.
				Ce service sera facturé 15€ pour les colis de 1 à 15kg.</p>
	</section>
</div>