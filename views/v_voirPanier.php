<script>
	function deleteFoieGras(rowToDeleteId, id){
		$.get("?action=delPanierAjax&id="+id,{},function(data){			
				//document.getElementById("contenuPanierTableau").deleteRow(numRow);				
				var el = document.getElementById(rowToDeleteId);
				el.parentNode.removeChild(el);
				
				if(data.emptyPanier==0){
					$("#totalArticle").empty().append(data.totalArticle + " ballotine(s)");
					$("#totalPanier").empty().append("_" + data.totalPanier + " €");
					$("#totalPanierTableau").empty().append("Total: " + data.totalPanier + " €");
				}else{
					document.getElementById("totalArticle").innerHTML="";
					document.getElementById("totalPanier").innerHTML="";
					document.getElementById("lienPanier").innerHTML="";
					document.getElementById("totalPanierTableau").innerHTML="";
					document.getElementById("totalPanierTableau").style.border='none';
					$("#totalArticle").empty().append("Votre panier est vide");
					$("#panierVideTd").empty().append("Votre panier est vide");
					$("#buttonCommande").hide();
					$("#infoFP").hide();
				}
		}, 'json');
		
	}
	
	function link(location){
		document.location.href="?action="+location;
	}
	
	function changeQuantite(plusMoins, idFg){
		if(plusMoins=="moins")
			val=parseInt($("#quantiteFg"+idFg).html())-1;
		else if(plusMoins=="plus")
			val=parseInt($("#quantiteFg"+idFg).html())+1;
		
		if(val<=5 && val>=1){	
			$("#quantiteFg"+idFg).empty().append(val);
		
			prix=parseInt($("#prixUnit"+idFg).html());
			$.get("?action=changeQuantiteAjax&id="+idFg+'&quantite='+val+'&prix='+prix, {}, function(data){						
					$("#totalArticle").empty().append(data.totalArticle + " ballotine(s)");
					$("#totalPanier").empty().append("_" + data.totalPanier + " €");
					$("#totalPanierTableau").empty().append("Total: " + data.totalPanier + " €");
					$("#totalFg"+idFg).empty().append(data.totalFg);
			}, 'json');
		}
	}
	
</script>

<div class="page" id=<?php echo $data['title'];?> >
    <header class="titrePage"><h1>Votre panier</h1></header>
	<table class='tableauPanier' id='contenuPanierTableau'>	
			<tr>
				<th>Foie gras</th>
				<th>Quantité</th>
				<th>Prix unitaire</th>
				<th>Prix total</th>
				<th><th>
	</tr>
	<?php
		if($_SESSION['panier']!=null){
			foreach($data['foieGrasPanier'] as $unFoieGras){
	?>	
				<tr id='rowFoieGras<?php echo $unFoieGras->getId(); ?>' >
					<td><?php echo $unFoieGras->getLibelle(). ' saveur ' .$unFoieGras->getSaveur(); ?></td>
					<td><img id="moins" src="views/images/moins_icon.png" alt="moins" onClick='changeQuantite("moins", <?php echo $unFoieGras->getId(); ?>)'>
						<span id='quantiteFg<?php echo $unFoieGras->getId(); ?>'><?php echo $_SESSION['panier'][$unFoieGras->getId()]; ?></span>
						<img id="plus" src="views/images/plus_icon.png" alt="plus" onClick='changeQuantite("plus", <?php echo $unFoieGras->getId(); ?>)'>
					</td>
					<td id='prixUnit<?php echo $unFoieGras->getId(); ?>'><?php echo $unFoieGras->getPrixKilo()/2; ?></td>
					<td id='totalFg<?php echo $unFoieGras->getId(); ?>'><?php echo ($unFoieGras->getPrixKilo()/2) * $_SESSION['panier'][$unFoieGras->getId()]; ?></td>
					<td><img class="croix" src="views/images/croix.png" alt="supprimerFoieGras" onClick='deleteFoieGras("rowFoieGras<?php echo $unFoieGras->getId(); ?>", <?php echo $unFoieGras->getId(); ?>)'></td>
				</tr>
		<?php } ?>
			<tr><td COLSPAN=5 id="panierVideTd"></td></tr>
	<?php	
		}else
			echo '<tr><td COLSPAN=5>Votre panier est vide</td></tr>';
	?>	
	</table>
	
	<?php
		if($_SESSION['panier']!=null)
			echo"<p id='infoFP'>* Le total du panier n'inclus pas les frais de port, pour plus d'information consulter les <a alt='Conditions' href='?action=paiementEtLivraison'>conditions de paiement et livraison</a>
				</p>
				<span id='totalPanierTableau'>Total: ". $totalPanier ." € *</span>		
				<span class='button' id='buttonCommande' onClick='link(\"passerCommande\")'>Passer commande</span>";
	?>
</div>