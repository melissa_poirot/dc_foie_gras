<script>
	$(function(){

		//changer le css en fontion des étapes en cours
		if(<?php echo "'".$data['debut_commande']."'"; ?>=='oui'){
			$('#etape_coord').show();
			var numEtape1=document.getElementById('numEtape1');
			var liEtape1=document.getElementById('liEtape1');
			numEtape1.style.color='#a43420';
			numEtape1.style.background='white';
			numEtape1.style.border='2px solid #a43420';
			liEtape1.style.fontSize='17px';
		}	
		
		verifForm();
		
	});
	
	function isEmail(emailStr){
		var checkTLD = 1;
		var knownDomsPat = /^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum|fr)$/;
		var emailPat = /^(.+)@(.+)$/;
		var specialChars = "\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
		var validChars = "\[^\\s" + specialChars + "\]";
		var quotedUser = "(\"[^\"]*\")";
		var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
		var atom = validChars + '+';
		var word = "(" + atom + "|" + quotedUser + ")";
		var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
		var domainPat = new RegExp("^" + atom + "(\\." + atom +")*$");
		var matchArray = emailStr.match(emailPat);
		if (matchArray == null) { return false; }
		var user = matchArray[1];
		var domain = matchArray[2];
		for (i=0; i<user.length; i++) {
			if (user.charCodeAt(i) > 127) { return false; }
		}
		for (i=0; i<domain.length; i++) {
			if (domain.charCodeAt(i) > 127) { return false; }
		}
		if (user.match(userPat) == null) { return false; }
		var IPArray=domain.match(ipDomainPat);
		if (IPArray != null) {
			for (var i=1; i<=4; i++) {
				if (IPArray[i] > 255) { return false; }
			}
			return true;
		}
		var atomPat = new RegExp("^" + atom + "$");
		var domArr = domain.split(".");
		var len = domArr.length;
		for (i=0; i<len; i++) {
			if (domArr[i].search(atomPat) == -1) { return false; }
		}
		if (checkTLD && domArr[domArr.length-1].length!=2 && domArr[domArr.length-1].search(knownDomsPat)==-1) { return false; }
		if (len < 2) { return false; }
		return true;
	}
	
	function isTel(telstr){
		var reg = new RegExp(/^(0|\+33)[1-9]([-. ]?[0-9]{2}){4}$/);
		return(reg.test(telstr));
	}
	
	
	//vérification du formulaire
	function verifForm(){
		var validNom, validPrenom, validMail, validTel, validAdresse, validVille, validCP= true;
	
		//vérification nom
		$('#nom').change(function(){
			//supprime tout les espace de la chaine
			if($('#nom').val().replace(/ /g,"")!=''){
				$("#erreurNom").fadeOut();	
				$('#imgErreurNom').hide();
				$('#imgValidNom').show();
				
				validNom=true;
			}else{
				$('#imgErreurNom').show();
				$('#imgValidNom').hide();
				$("#erreurNom").fadeIn().text("Veuillez compléter ce champ");		
				
				validNom=false;
			}
		});

		//vérification prenom
		$('#prenom').change(function(){
			if($('#prenom').val().replace(/ /g,"")!=''){
				$("#erreurPrenom").fadeOut();	
				$('#imgErreurPrenom').hide();
				$('#imgValidPrenom').show();
				
				validPrenom=true;
			}else{
				$('#imgErreurPrenom').show();
				$('#imgValidPrenom').hide();
				$("#erreurPrenom").fadeIn().text("Veuillez compléter ce champ");

				validPrenom=false;
			}
		});
		
		//vérification mail
		$('#mail').change(function(){
			if(!(isEmail(document.forms['formCoord'].elements['mail'].value)) && $('#mail').val().replace(/ /g,"")!=''){
				$('#imgErreurMail').show();
				$("#erreurMail").fadeIn().text("Votre e-mail est incorrect");
				$('#imgValidMail').hide();
				
				validMail=false;
			}else{
				if($('#mail').val().replace(/ /g,"")!=''){
					$("#erreurMail").fadeOut();	
					$('#imgErreurMail').hide();
					$('#imgValidMail').show();
					
					validMail=true;
				}else{
					$('#imgErreurMail').show();
					$('#imgValidMail').hide();
					$("#erreurMail").fadeIn().text("Veuillez compléter ce champ");
					
					validMail=false;
				}
			}
		});
		
		//vérification ville
		$('#ville').change(function(){
			$.get("?action=autocompletion&villeTest="+ $('#ville').val() +"&cp="+ $('#cp').val(),{},function(data){	
				if(data.existVille && $('#ville').val().replace(/ /g,"")!=''){
					$("#erreurVille").fadeOut();	
					$('#imgErreurVille').hide();
					$('#imgValidVille').show();
				
					//changement pour le code postal
					if($('#cp').val() != ""){
						if(data.villeMatchCP != false){
							$("#erreurCP").fadeOut();	
							$('#imgErreurCP').hide();
							$('#imgValidCP').show();
						}else{
							$("#erreurCP").fadeIn();	
							$('#imgErreurCP').show();
							$('#imgValidCP').hide();
						}
					}
					
					//valeur frais de port
					$('#fraisPort').val(data.fraisPort);
					
					var totalFP=parseInt(<?php echo $totalPanier; ?>)+ parseInt($('#fraisPort').val());
					
					//tableau recap
					$('#totalPanierTableauFP').empty().append("Total: "+ totalFP +" €");
					$('#fraisPortTab').empty().append("Frais de port: "+$('#fraisPort').val() + " €");
					//paypal
					document.getElementById("amount").value = totalFP;
					document.getElementById("shipping").value = $('#fraisPort').val();
					
					validVille=true;
				}else if($('#ville').val().replace(/ /g,"")==''){
					$('#imgErreurVille').show();
					$('#imgValidVille').hide();
					$("#erreurVille").fadeIn().text("Veuillez compléter ce champ");		
				
					validVille=false;
				}else if(!(data.existVille)){
					$('#imgErreurVille').show();
					$('#imgValidVille').hide();
					$("#erreurVille").fadeIn().text("Votre nom de ville est incorrect");		
				
					validVille=false;
				}
					
			}, 'json');
		});
		
		//vérification tel
		$('#tel').change(function(){
			if(!(isTel(document.forms['formCoord'].elements['tel'].value)) && $('#tel').val().replace(/ /g,"")!=''){				
				$('#imgErreurTel').show();
				$("#erreurTel").fadeIn().text("Votre numéro de téléphone est incorrect");
				$('#imgValidTel').hide();
				
				validTel=false;
			}else{
				if($('#tel').val().replace(/ /g,"")!=''){			
					$("#erreurTel").fadeOut();
					$('#imgValidTel').show();
					$('#imgErreurTel').hide();
					
					validTel=true;
				}else{
					$('#imgErreurTel').show();
					$('#imgValidTel').hide();
					$("#erreurTel").fadeIn().text("Veuillez compléter ce champ");
				
					validTel=false;
				}
			}
		});
		
		
		//vérification num rue
		$('#adresse').change(function(){			
			var str= document.forms['formCoord'].elements['adresse'].value;
			var tab= str.split(" ");	
			if(isNaN(tab[0]) && $('#adresse').val().replace(/ /g,"")!=''){
				$('#imgErreurAdresse').show();
				$("#erreurAdresse").fadeIn().text("Votre adresse est incorrecte");
				$('#imgValidAdresse').hide();
				
				validAdresse=false;
			}
			else{
				if($('#adresse').val().replace(/ /g,"")!=''){
					$("#erreurAdresse").fadeOut();	
					$('#imgErreurAdresse').hide();
					$('#imgValidAdresse').show();
					
					validAdresse=true;
				}else{
					$('#imgErreurAdresse').show();
					$('#imgValidAdresse').hide();
					$("#erreurAdresse").fadeIn().text("Veuillez compléter ce champ");
					
					validAdresse=false;
				}
			}
		});
		
		
		//verification code postal
		$('#cp').change(function(){	
			$.get("?action=autocompletion&cpTest="+ $('#cp').val() +"&villecp="+ $('#ville').val(),{},function(data){	
				if(!(isNaN($('#cp').val())) && $('#cp').val().length==5 && data.villeMatchCP !=false && $('#cp').val().replace(/ /g,"")!=''){
					$("#erreurCP").fadeOut();	
					$('#imgErreurCP').hide();
					$('#imgValidCP').show();
				
					validCP=true;
				}else if((isNaN($('#cp').val()) || $('#cp').val().length!=5) && $('#cp').val().replace(/ /g,"")!=''){
					$('#imgErreurCP').show();
					$("#erreurCP").fadeIn().text("Votre code postal est incorrect");
					$('#imgValidCP').hide();
					
					validCP=false;
				}else if($('#cp').val().replace(/ /g,"")==''){
					$('#imgErreurCP').show();
					$('#imgValidCP').hide();
					$("#erreurCP").fadeIn().text("Veuillez compléter ce champ");		
				
					validCP=false;
				}else if(!(data.existCP)){
					$('#imgErreurCP').show();
					$('#imgValidCP').hide();
					$("#erreurCP").fadeIn().text("Votre code postal est incorrect");		
				
					validCP=false;
				}
					
			}, 'json');		

		});
		
		//autocompletion de la ville
		$('#ville').autocomplete({
			source : function(requete, reponse){ // requete, reponse = données nécessaires au plugin
				$.ajax({
					url : '?action=autocompletion', // on appelle le script JSON
					dataType : 'json', // on spécifie bien que le type de données est en JSON
					data : {
						ville_firstChar : $('#ville').val(), // on donne la chaîne de caractère tapée dans le champ de recherche
						maxRows : 15
					},

					success : function(donnee){
						console.log(donnee);
						reponse($.map(donnee, function(objet){
							console.log(objet);
							return objet; // on retourne cette forme de suggestion
						}));
					}
				});
			},

			minLength : 3,
			select : function(event, ui){ // lors de la sélection d'une ville
				$.get("?action=autocompletion&ville="+ui.item.value,{},function(data){	
					$("#cp").val(data.cp);
					$("#erreurCP").fadeOut();	
					$('#imgErreurCP').hide();
					$('#imgValidCP').show();
					
					validCP=true;
					
					$("#erreurVille").fadeOut();	
					$('#imgErreurVille').hide();
					$('#imgValidVille').show();
					
					validVille=true;
					}, 'json');
			}
		});
	
		$("#formCoord").submit(function(e){
			//pas de rechargement de page
			e.preventDefault();			
			if(validMail && validTel && validAdresse && validCP && validNom && validPrenom && validVille){
				//changer étape ->paiement
				$('#etape_coord').hide();
				$('#etape_cmd').show();
				var numEtape2=document.getElementById('numEtape2');
				var liEtape2=document.getElementById('liEtape2');
				numEtape2.style.color='#a43420';
				numEtape2.style.background='white';
				numEtape2.style.border='2px solid #a43420';
				liEtape2.style.fontSize='17px';
				var numEtape1=document.getElementById('numEtape1');
				var liEtape1=document.getElementById('liEtape1');
				numEtape1.style.color='white';
				numEtape1.style.background='#a43420';
				numEtape1.style.border='none';
				liEtape1.style.fontSize='16px';
				
				//insert or update client
				//si nom, prenom, adresse mail pas dans la base insert sinon update + return id cli
				$.get("?action=insertClient&nomCli="+ $('#nom').val() +"&prenomCli="+ $('#prenom').val() +"&mailCli="+ $('#mail').val() +
				"&telCli="+ $('#tel').val() + "&adresseCli="+ $('#adresse').val() +"&cmpAdresseCli="+ $('#cmpAdresse').val() +
				"&villeCli="+ $('#ville').val() +"&codePCli="+ $('#cp').val(),{},function(data){}, 'json');
			}else 
				$('#alertFormError').show();
		});
	}
	
	function changeEtape(nomEtape){
		
	
		
		if(nomEtape=="coord"){
			$('#etape_coord').show();
			$('#etape_cmd').hide();
			var numEtape1=document.getElementById('numEtape1');
			var liEtape1=document.getElementById('liEtape1');
			numEtape1.style.color='#a43420';
			numEtape1.style.background='white';
			numEtape1.style.border='2px solid #a43420';
			liEtape1.style.fontSize='17px';
			var numEtape2=document.getElementById('numEtape2');
			var liEtape2=document.getElementById('liEtape2');
			numEtape2.style.color='white';
			numEtape2.style.background='#a43420';
			numEtape2.style.border='none';
			liEtape2.style.fontSize='16px';
		}else if(nomEtape=='commande'){
			
			$('#etape_cmd').show();
			$('#etape_pmnt').hide();
			var numEtape2=document.getElementById('numEtape2');
			var liEtape2=document.getElementById('liEtape2');
			numEtape2.style.color='#a43420';
			numEtape2.style.background='white';
			numEtape2.style.border='2px solid #a43420';
			liEtape2.style.fontSize='17px';
			var numEtape3=document.getElementById('numEtape3');
			var liEtape3=document.getElementById('liEtape3');
			numEtape3.style.color='white';
			numEtape3.style.background='#a43420';
			numEtape3.style.border='none';
			liEtape3.style.fontSize='16px';
		}else if(nomEtape=='paiement'){
			var total = parseInt(<?php echo $totalPanier; ?>)+ parseInt($('#fraisPort').val());
			//faire le insert pour la commande
			$.get("?action=insertCommande&total=" + total,{},function(data){	
			
					}, 'json');
			
			$('#etape_pmnt').show();
			$('#etape_cmd').hide();
			var numEtape3=document.getElementById('numEtape3');
			var liEtape3=document.getElementById('liEtape3');
			numEtape3.style.color='#a43420';
			numEtape3.style.background='white';
			numEtape3.style.border='2px solid #a43420';
			liEtape3.style.fontSize='17px';
			var numEtape2=document.getElementById('numEtape2');
			var liEtape2=document.getElementById('liEtape2');
			numEtape2.style.color='white';
			numEtape2.style.background='#a43420';
			numEtape2.style.border='none';
			liEtape2.style.fontSize='16px';
		}
		
	}
	
	function hideAlert(){
		$('#alertFormError').hide();
	}
</script>


<div class="page" id=<?php echo $data['title'];?> >
	<header class="titrePage">
		<h1>Commande</h1>
		<ul>
			<span class='numberEtp' id="numEtape1">1</span><li id="liEtape1">Vos coordonnées</li>
			<span class='numberEtp' id="numEtape2">2</span><li id="liEtape2">Votre commande</li>
			<span class='numberEtp' id="numEtape3">3</span><li id="liEtape3">Paiement</li>
			<span class='numberEtp' id="numEtape4">4</span><li id="liEtape4">Validation</li>
		</ul>
	</header>
	

	<section class='contener' id="etape_coord" hidden >
		<form action="index.php?action=passerCommande" method="post" id='formCoord'>
		<fieldset>
				<legend>Coordonnées personnelles</legend>
				<label for="nom">Nom*</label>
				<input type="text" name="nom" id="nom" required="required"/>
				<img class="imgErreur" id='imgErreurNom' src="views/images/croix_erreur.png" alt="imageErreur" hidden />
				<img class="imgValid" id='imgValidNom' src="views/images/ok.png" alt="imageValid" hidden />
				<span class='erreur' id='erreurNom'></span></br>
				<label for="prenom">Prénom*</label>
				<input type="text" name="prenom" id="prenom" required="required"/>
				<img class="imgErreur" id='imgErreurPrenom' src="views/images/croix_erreur.png" alt="imageErreur" hidden />
				<img class="imgValid" id='imgValidPrenom' src="views/images/ok.png" alt="imageValid" hidden />
				<span class='erreur' id='erreurPrenom'></span></br>
				<label for="mail">E-Mail*</label>
				<input type="email" name="mail" id="mail" required="required"></code>
				<img class="imgErreur" id='imgErreurMail' src="views/images/croix_erreur.png" alt="imageErreur" hidden />
				<img class="imgValid" id='imgValidMail' src="views/images/ok.png" alt="imageValid" hidden />
				<span class='erreur' id='erreurMail'></span></br>
				<label for="tel">Téléphone*</label>
				<input  type="tel" name="tel" id="tel" required="required"></code>
				<img class="imgErreur" id='imgErreurTel' src="views/images/croix_erreur.png" alt="imageErreur" hidden />
				<img class="imgValid" id='imgValidTel' src="views/images/ok.png" alt="imageValid" hidden />
				<span class='erreur' id='erreurTel'></span></br>
		</fieldset>
		<fieldset>
				<legend>Adresse de livraison</legend>
				<label for="adresse">N° et nom de rue*</label>
				<input type="text" name="adresse" id="adresse" required="required"/>
				<img class="imgErreur" id='imgErreurAdresse' src="views/images/croix_erreur.png" alt="imageErreur" hidden />
				<img class="imgValid" id='imgValidAdresse' src="views/images/ok.png" alt="imageValid" hidden />
				<span class='erreur' id='erreurAdresse'></span></br>
				<label for="cmpAdresse">Compléments d'adresse</label>
				<input type="text" name="cmpAdresse" id="cmpAdresse" /></br>
				<label for="ville">Ville*</label>
				<input type="text" name="ville" id="ville" required="required"/>
				<img class="imgErreur" id='imgErreurVille' src="views/images/croix_erreur.png" alt="imageErreur" hidden />
				<img class="imgValid" id='imgValidVille' src="views/images/ok.png" alt="imageValid" hidden />
				<span class='erreur' id='erreurVille'></span></br>
				<label for="cp">Code postal*</label>
				<input type="text" name="cp" id="cp" required="required"/>
				<img class="imgErreur" id='imgErreurCP' src="views/images/croix_erreur.png" alt="imageErreur" hidden />
				<img class="imgValid" id='imgValidCP' src="views/images/ok.png" alt="imageValid" hidden />
				<span class='erreur' id='erreurCP'></span></br>
				<input  type="text" name="fraisPort" id="fraisPort" hidden>
			</fieldset>
			<p id='champsOb'>* champs obligatoires</p>
			<input id='buttonValidForm' class='button' type="submit" value="Valider"></code>
		</form>
	</section>
	<section class='contener' id="etape_cmd" hidden >
	<table class='tableauPanier'>	
			<tr>
				<th>Foie gras</th>
				<th>Quantité</th>
				<th>Prix unitaire</th>
				<th>Prix total</th>
		</tr>
	<?php
		foreach($data['foieGrasPanier'] as $unFoieGras){
	?>	
			<tr id='rowFoieGras<?php echo $unFoieGras->getId(); ?>' >
				<td><?php echo $unFoieGras->getLibelle(). ' saveur ' .$unFoieGras->getSaveur(); ?></td>
				<td><span><?php echo $_SESSION['panier'][$unFoieGras->getId()]; ?></span></td>
				<td><?php echo $unFoieGras->getPrixKilo()/2; ?></td>
				<td><?php echo ($unFoieGras->getPrixKilo()/2) * $_SESSION['panier'][$unFoieGras->getId()]; ?></td>
			</tr>
		<?php } ?>
			<tr>
				<td COLSPAN=2></td>
				<td COLSPAN=2 id='fraisPortTab'></td>
			</tr>
		</table>
		<span id='totalPanierTableauFP'></span>
		<span class='button' id='buttonValidCommande' onClick='changeEtape("paiement")'>Valider</span>
		<span class='button' id='buttonRetourCoord' onClick='changeEtape("coord")'>Retour</span>
	</section>
	
	<section class='contener' id="etape_pmnt" hidden >		
		<h3>Choisissez votre mode paiement</h3>
		<!--- URL PROD -> https://www.paypal.com/cgi-bin/webscr -->
		<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">

			<!--Total panier -->
			<input id='amount' name="amount" type="hidden" value="" />
			<!--Indication de la devise -->
			<input name="currency_code" type="hidden" value="EUR" />
			<!--Indication du montant des frais de port -->
			<input id='shipping' name="shipping" type="hidden" value="" />
			<!--Indication du montant de la TVA (ou 0.00) -->
			<input name="tax" type="hidden" value="0.00" />
			<!--Indication de l'URL de retour automatique -->
			<input name="return" type="hidden" value="http://localhost/foie_gras/?action=accueil" />
			<!--Indication de l'URL de retour si annulation du paiement -->
			<input name="cancel_return" type="hidden" value="http://localhost/foie_gras/?action=consulterPanier" />
			<!--Indication de l'URL de retour pour contrôler le paiement -->
			<input name="notify_url" type="hidden" value="http://localhost/foie_gras/?action=consulterPanier" />
			<!--Indication du type d'action -->
			<input name="cmd" type="hidden" value="_xclick" />
			<!--Indication de l'adresse e-mail test du vendeur (a remplacer par l'e-mail de votre compte Paypal en production) -->
			<input name="business" type="hidden" value="melissa.toriop@gmail.com" />
			<!--Indication du libellé de la commande qui apparaitra sur Paypal -->
			<input name="item_name" type="hidden" value="Foie gras frais" />
			<!--Indication permettant à l'acheteur de laisser un message lors du paiement -->
			<input name="no_note" type="hidden" value="1" />
			<!--Indication de la langue -->
			<input name="lc" type="hidden" value="FR" />
			<!--Indication du type de paiement -->
			<input name="bn" type="hidden" value="PP-BuyNowBF" />
			<!--Indication du numéro de la commande (très important) value=' echo $_SESSION['panier']['commande']; ' -->
			<input name="custom" type="hidden" value="$_SESSION['panier']['idCommande']" />
			<!--Bouton pour valider le paiement -->
			<input class="button" type="submit" value="Par PayPal" />

		</form>
		
		<span class='button' id='buttonRetourCmd' onClick='changeEtape("commande")'>Retour</span>
	</section>
</div>

<div id="alertFormError" hidden>
	<img class="croix" src="views/images/croix.png" alt="croix" onClick='hideAlert()'>
	<p>Veuillez vérifier vos coordonnées</p>	
	<span class='button' onClick='hideAlert()'>
		OK
	</span>
</div>